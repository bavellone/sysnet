/**
 * Models an inode in a file system.
 *
 * @author Thomas Reichherzer
 * @author Ben Avellone
 * @author Hunter Hardy
 */

#include "inode.h"

    // complete this code

void initializeInode(inode_t inode)
{
	int i;

	inode->flags = FLAG_UNUSED;
	inode->filesize = 0;
	inode->filename[0] = '\0';

    for (i=0; i < MAX_POINTERS; i++)
    {
        inode->pointer[i] = 0;
    }
}


inode_t createInode() {
	inode_t inode = malloc(sizeof(inode_t*));
	return inode;
}

inode_t destroyInode(inode_t inode) {
	free(inode);
	return NULL;
}
