#ifndef INODE_H_
#define INODE_H_

/**
 * Models an inode in a file system.
 *
 * @author Thomas Reichherzer
 * @author Ben Avellone
 * @author Hunter Hardy
 */
 #include <stdio.h>
 #include <stdlib.h>
 
#define MAX_POINTERS 250

#define FLAG_UNUSED 0
#define FLAG_DIRECTORY 1
#define FLAG_FILE 2


// indicates if the inode points to a file or a directory
typedef struct inode
{
	 // indicates if the inode points to a file or a directory
	 // 0 indicates that the Inode is unused
    int flags;
    // indicates the size of the file
    int filesize;
	 // specifies the filename (up to 8 characters plus . plus 3 letter extension is allowed)
	 char filename[12];
    // a list of direct pointers to data blocks of the file
    int pointer[MAX_POINTERS];
} *inode_t;

//Prototypes
void initializeInode(inode_t);
inode_t createInode();
inode_t destroyInode(inode_t);
#endif /*INODE_H_*/
