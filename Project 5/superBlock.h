#ifndef SUPERBLOCK_H_
#define SUPERBLOCK_H_

/**
 * Models a super block in a file system.
 *
 * @author Thomas Reichherzer
 * @author Ben Avellone
 * @author Hunter Hardy
 */

typedef struct superBlock
{
	int numberOfDiskBlocks;
	int numberOfInodes;
	int firstBlockOfFreeList;
} *superBlock_t;

#endif /*SUPERBLOCK_H_*/
