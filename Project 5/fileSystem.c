/**
 * Emulates a file system.
 *
 * @author Thomas Reichherzer
 * @author Ben Avellone
 * @author Hunter Hardy
 */

#include "fileSystem.h"

/**
 * A test program for testing file system operations.
 *
 * @param args - a list of arguments
*/
int main(int argc, char* argv[])
{
	//int error = format( 100, "./disk1.bin" );

	return 0;
}

int format( int diskSizeInKB, char* path )
{
	int error, i;
  // create a disk
  error = createDisk(diskSizeInKB, path);

  // complete implementation of this function
  // you must use the functions read/writeSuperblock() & read/writeInode()
	// formatting requires that you setup the superblock, write empty inodes,
	// and setup your free list for the remaining blocks

	superBlock_t superBlock = malloc(sizeof(superBlock_t));

	superBlock->numberOfDiskBlocks = diskSizeInKB;
	superBlock->numberOfInodes = NUM_INODES;
	superBlock->firstBlockOfFreeList = NUM_INODES + 1;

	writeSuperBlock(superBlock);


	// Create an inode to write to the disk
	inode_t inode = createInode();
	initializeInode(inode);
	for (i = 0; i < NUM_INODES; i++) {
		// Write the empty inode to disk
		writeInode(inode, NUM_INODES + i + 1);
	}
	// We are done with this inode
	destroyInode(inode);


	int numDataBlocks = superBlock->numberOfDiskBlocks - superBlock->numberOfInodes - 1;
	char buffer[BLOCK_SIZE];

	// Setup free list
	for (i = superBlock->firstBlockOfFreeList; i < numDataBlocks; i++) {
		intToByteArray(i, buffer, 0);
		writeBlock(i, buffer, 4);
	}

	intToByteArray(-1, buffer, 0);
	writeBlock(numDataBlocks, buffer, 4);

	return error;
}

void setupFreeList(superBlock_t superBlock)

/**
 * Opens a binary file on the disk for storing data.
 */
fd_t openf(char* name) {
	int i;
	inode_t found = NULL;
	fd_t fileDescriptor = NULL;

	for (i = 1; i <= NUM_INODES; i++) {
		inode_t inode = createInode();
		readInode(inode, i);
		if (strcmp(inode->filename, name) == 0) {
			found = inode;
			break;
		}
	}

	if (found != NULL) {
		fileDescriptor = malloc(sizeof(fd_t*));
		fileDescriptor->inode = inode;
		fileDescriptor->fileptr = 0;
	}

	return fileDescriptor;
}

int closef(fd_t stream) {
	free(stream->inode);
	free(stream);
	return 0;
}

int readf(fd_t stream, char* data, int size) {
	int numBlocksToRead = 1 + (size / BLOCK_SIZE);
	char buffer[BLOCK_SIZE];
	int i, error = 0;


	for (i = 0; i < numBlocksToRead; i++) {
		error = readBlock(stream->inode->pointer[0] + i, buffer);
		if (error == -1) break; // An error occurred, break out of loop

		strncpy(data[i * BLOCK_SIZE], buffer, BLOCK_SIZE);
	}

	return error;
}

int writef(fd_t stream, char* data, int size) {
	int numBlocksToWrite = 1 + (size / BLOCK_SIZE);
	char buffer[BLOCK_SIZE];
	int i, error = 0;

	for (i = 0; i < numBlocksToWrite; i++) {
		strncpy(buffer, data[i * BLOCK_SIZE], BLOCK_SIZE);

		error = writeBlock(stream->inode->pointer[0] + i, buffer);
		if (error == -1) break; // An error occurred, break out of loop
	}

	return error;
}

/**
    * Reads the super block from disk.
    *
    * @param - destination to store the disk's super block
    */
int readSuperBlock( superBlock_t* superBlock )
{
  int error;
	char buffer[BLOCK_SIZE];

  // read block and convert it to the superblock
	error = readBlock(0, buffer);

	// read number of disk blocks from block
	superBlock->numDiskBlocks = byteArrayToInt(buffer, 0 );

	// read number of inodes from block
	superBlock->numInodes =  byteArrayToInt(buffer, 4 );

	// read start of free block to disk
	superBlock->firstBlockOfFreeList = byteArrayToInt(buffer, 8 );

	return error;
}

/**
 * Writes the super block to disk.
 *
 * @param superBlock - the disk's super block
 */
int writeSuperBlock(superBlock_t* superBlock )
{
	// setup buffer to be written to disk
	char buffer[BLOCK_SIZE];

	// write number of disk blocks into buffer
	intToByteArray( superBlock->numberOfDiskBlocks, buffer, 0);

	// write number of inodes into buffer
	intToByteArray( superBlock->numberOfInodes, buffer, 4);

	// write number of inodes into buffer
	intToByteArray( superBlock->firstBlockOfFreeList, buffer, 8);


	return writeBlock(0, buffer, 12);
}

/**
 * Reads an Inode from disk.
 *
 * @param inode - the inode to read into
 * @param blockNumber - the block number that holds the inode to be read
 *
*/
void readInode(inode_t* inode, int blockNumber)
{
	int error;
	char buffer[BLOCK_SIZE];
	int i, pos;
	// read block containing the Inode
  error = readBlock(blockNumber, buffer);

	inode->flags = byteArrayToInt(buffer, 0);
	inode->filesize = byteArrayToInt(buffer, 4);
	strncpy(inode->filename, buffer[8], 12);

	for (i = 0, pos = 20; i < MAX_POINTERS && pos <= BLOCK_SIZE - 4; i++) {
		inode->pointer[i] = byteArrayToInt(buffer, pos);
		pos = i * 4 + 20;
	}
}

/**
 * Writes an Inode to disk.
 *
 * @param inode - the inode to be written to disk
 * @param blockNumber - the block number in which the inode will be written
 *
*/
void writeInode(inode_t* inode, int blockNumber)
{
	char buffer[BLOCK_SIZE];
	int i, pos;

	intToByteArray( inode->flags, buffer, 0);
	intToByteArray( inode->filesize, buffer, 4);
	strncpy(buffer[8], inode->filename, 12);

	for (i = 0, pos = 20; i < MAX_POINTERS, pos <= BLOCK_SIZE - 4; i++) {
		intToByteArray(inode->pointer[i], buffer, pos);
		pos = i * 4 + 20;
	}

	return writeBlock(blockNumber, buffer);
}

/**
 * Prints the content of the Inodes of the virtual disk to the screen.
 */
void printInodes()
{
	for (int i = 1; i <= NUM_INODES; i++) {
		printf("The flag is %i\n", inode->flag);
		printf("The owner is %i\n", inode->owner);
		printf("The filesize is %i\n", inode->filesize);
		printf("The filename is %s\n", inode->filename);
		printf("The list of pointers is:\t");

		for(int j = 0; j < MAX_POINTERS; j++){
			if(inode->pointer[j] == 0)
				break;
			else
				printf(" %i",inode->pointer[j]);
		}
	}
}

/**
 * Prints the block numbers of disk blocks that are free.
 */
void printFreeBlocks()
{
	// implement this function
}


/**
 * Writes an integer to a byte array.
 *
 * @param value - the integer to be written to a byte array
 * @param buffer - the byte array in which the integer will be written
 * @param pos - the position in the byte array where an integer will be written
 */
void intToByteArray( int value, char* buffer, int pos )
{
	int i;

    for (i = 0; i < 4; i++)
		{
        int shift = (4 - 1 - i) * 8;
        buffer[i+pos] = (char) (( (unsigned int) value >> shift) & 0xFF);
    }
}

 /**
  * Reads an integer from a byte array at a specific location.
  *
  * @param buffer - the byte array from which an integer will be read
  * @param pos - the position in the array from where an integer will be read
  *
  * @return an integer read from the byte array
 */
int byteArrayToInt( char* buffer, int pos )
{
	int i, value = 0;

    for (i = 0; i < 4; i++)
	{
		int shift = (4 - 1 - i) * 8;
        value += (buffer[i + pos] & 0x000000FF) << shift;
    }
    return value;
}
