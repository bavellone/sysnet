#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

/**
 * Emulates a file system.
 *
 * @author Thomas Reichherzer
 * @author Ben Avellone
 * @author Hunter Hardy
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "disk.h"
#include "superBlock.h"

#define NUM_INODES 25 //To be filled in

typedef struct fd
{
	inode_t inode;
	int fileptr;
} *fd_t;

//Prototypes

/**
 * Formats the disk.
 *
 * @param diskSizeInKB - the size of the disk in KB
 * @param path - the path of the virtual disk
 *
 * @returns -1 if an error occurs
*/
int format( int diskSizeInKB, char* path );

/**
 * Opens a file by the specified name and returns a file descriptor.
 *
 * @param name - the name of the file to be opened
 *
 * @returns a file descriptor if the file was successfully opened or NULL, if an error occurred
 */
fd_t openf(char* name);

/**
 * Closes the file given the file descriptor.
 *
 * @returns -1 if an error occurs
 */
int closef(fd_t stream);

/**
 * Reads data from a file stream and stores them at the specified location.
 *
 * @param stream - the file stream to be read
 * @param data - the buffer to store data in
 * @param size - the number of bytes to be read
 *
 * @returns -1 if an error ocurred and 0 otherwise
 */
int readf(fd_t stream, char* data, int size);

/**
 * Writes data to a file stream given a file descriptor and data to be written.
 *
 * @param stream - the file stream to append data
 * @param data - the buffer with data to be written to a file
 * @param size - the number of bytes to be written
 *
 * @returns -1, if an error occurred and 0 otherwise
 */
int writef(fd_t stream, char* data, int size);

void readSuperBlock( superBlock_t* );
void writeSuperBlock( superBlock_t* );
void readInode( inode_t*, int );
void writeInode( inode_t*, int );
void printInodes();
void printFreeBlocks();
void intToByteArray( int, char*, int );
int byteArrayToInt( char*, int );

#endif /*FILESYSTEM_H_*/
