/**
* @file thread.c
* @brief Prototypes and struct definition for our ThreadManager
*
* @author Ben Avellone
* @author Hunter Hardy
*
*/

#ifndef MY_THREADS

#define MY_THREADS 1

#include <pthread.h>

typedef struct thread_manager {
    int threads;
    pthread_t *threadIDs;

} *ThreadManager;

ThreadManager createThreadManager(int);
void createThreads(ThreadManager, void *, void **);
void joinThreads(ThreadManager);
ThreadManager freeThreadManager(ThreadManager);

#endif
