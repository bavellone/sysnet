#!/bin/bash
outputFile=testDataTiming.csv
NArr=( 10000000 )
ThreadArr=( 1 2 4 8 10 12 16 32 )
Ncount=0
Tcount=0
Nmax=1
Tmax=8

echo "Running tests..."
while [ ${Ncount} -lt ${Nmax} ]; do
	N=${NArr[Ncount]}
	Tcount=0
	while [ ${Tcount} -lt ${Tmax} ]; do
		T=${ThreadArr[Tcount]}
		#FILE=testData_${N}_${T}.csv
		echo "N:$N T:$T"
		./mt-collatz ${N} ${T} > /dev/null 2>> ${outputFile}
		let Tcount=Tcount+1
	done
	let Ncount=Ncount+1
done

echo "Test results output to: $outputFile"
