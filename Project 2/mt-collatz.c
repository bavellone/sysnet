/**
* @file mt-collatz.c
* @brief This file contains an implementation of the Collatz algorithm solved using multiple threads.
*
* This program will record the time it takes to compute the Collatz algorithm using a given amount
* of threads and a given N. Each thread will receive a ThreadLocal struct, which contains the range
* that particular thread must compute.
*
* At the end of the program, the results from the benchmark will be output.
*
* @author Ben Avellone
* @author Hunter Hardy
*
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include "thread.h"
#include "mt-collatz.h"

int Debug = 0;
int collatzBins[NUM_COLLATZ_BINS] = {0};
long long maxCollatz;

/**
* @brief The high-level flow of our program execution
*/
int main(int argc, char *argv[]) {
    int threads = initCollatz(argc, argv);

    ThreadManager tm = createThreadManager(threads);
	ThreadLocal *ctx = createContexts(tm->threads);

    float elapsed = benchmark(tm, ctx);

    tm = freeThreadManager(tm);
	ctx = freeContexts(ctx, threads);

    printBins();
    printStats(maxCollatz, threads, elapsed);

    return 0;
}

/**
* @brief Retrieves value for N and T. Prints errors as necessary.
* @return T - the number of threads to spawn
*/
int initCollatz(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "This program requires two arguments: ./mt-collatz [Max Range] [Threads]\n");
        exit(1);
    }

    if (argc == 4 && strcmp(argv[3], "-Debug") == 0)
        Debug = 1;

    maxCollatz = atoi(argv[1]);

    return atoi(argv[2]);
}

/**
* @brief Reads the realtime clock and populates the provided struct
*/
void readTime(struct timespec timeStruct) {
    if (clock_gettime(CLOCK_REALTIME, &timeStruct) == -1) {
        fprintf(stderr, "Could not read real-time clock!");
    }
}

/**
* @brief Creates worker threads and dispatches them.
* @return The length of time the algorithm runs for
*/
float benchmark(ThreadManager tm, ThreadLocal *ctx) {
    // Setup timing structs and read clock
    struct timespec start, stop;
    readTime(start);

    createThreads(tm, &dispatch, (void**)ctx);
    joinThreads(tm);

    readTime(stop);

    return (stop.tv_sec - start.tv_sec) + (stop.tv_nsec - start.tv_nsec) / BILLION;
}

/**
* @brief Checks if the number is even
* @return True if num is even
*/
long long isEven(long long num) {
    return (num % 2 == 0);
}

/**
* @brief Performs the odd function of the Collatz algorithm
* @return The next number in the Collatz sequence
*/
long long collatzOdd(long long num) {
    return (num * 3) + 1;
}

/**
* @brief Performs the even function of the Collatz algorithm
* @return The next number in the Collatz sequence
*/
long long collatzEven(long long num) {
    return num / 2;
}

/**
* @brief The entry point of the worker threads
*/
void *dispatch(void *threadData) {
	ThreadLocal data = (ThreadLocal) threadData;
	long long processing = data->start;
	while (processing <= data->stop) {
		int stop = nextCollatzNumber(processing++, 0);
		collatzBins[stop]++;
	}
	pthread_exit(0);
	return 0;
}

/**
* @brief Creates a ThreadLocal struct which contains run-time data for a single thread
*
* This function uses the position of the thread relative to the number of other threads
* to determine the range of the Collatz sequence to be computed.
*
* Since the value of threadNum will be unique for each thread, we use that to our
* advantage when calculating the range that thread should compute. In this manner,
* we avoid race conditions.
*
* @return The ThreadLocal struct
*/
ThreadLocal createThreadLocal(int threadNum, int threadMax) {
	ThreadLocal tl = malloc(sizeof(ThreadLocal*));

	long long interval = maxCollatz / threadMax;

	tl->start = threadNum * interval;
	tl->stop = tl->start + interval;

	// Add an offset to the start range so we don't duplicate results
	tl->start += 1;

	// If we are the last thread, calculate the remaining values of N
	if (threadNum + 1 == threadMax)
		tl->stop = maxCollatz;

	if (Debug) {
		fprintf(stderr, "Thread: [%i]\t", threadNum);
        fprintf(stderr, "Start: [%lli]\t", tl->start);
        fprintf(stderr, "Stop: [%lli]\n", tl->stop);
	}

	return tl;
}

/**
* @brief Frees memory of a ThreadLocal
* @return Null
*/
ThreadLocal freeThreadLocal(ThreadLocal tl) {
	free(tl);
	return NULL;
}

/**
* @brief Creates the number of ThreadLocal structs as specified by threads
* @return An array of ThreadLocal structs
*/
ThreadLocal *createContexts(int threads) {
	int i;
	ThreadLocal *ctx = malloc(sizeof(ThreadLocal*) * threads);

	for (i = 0; i < threads; i++)
		ctx[i] = createThreadLocal(i, threads);

	return ctx;
}

/**
* @brief Frees memory of a ThreadLocal array
* @return Null
*/
ThreadLocal *freeContexts(ThreadLocal *ctx, int threads) {
	int i;
	for (i = 0; i < threads; i++)
		ctx[i] = freeThreadLocal(ctx[i]);

	free(ctx);

	return NULL;
}

/**
* @brief A recursive function that will recurse until the sequence reaches 1
* @return The stopping number for this sequence
*/
long long nextCollatzNumber(long long num, int stop) {
	if (num == 1)
		return stop;
	if (isEven(num))
		return nextCollatzNumber(collatzEven(num), ++stop);
	else
		return nextCollatzNumber(collatzOdd(num), ++stop);
}

/**
* @brief Prints the frequencies of the stopping times
*/
void printBins() {
	int i;
	for (i = 0; i < NUM_COLLATZ_BINS; i++) {
		printf("%i,%i\n", i, collatzBins[i]);
	}
}

/**
* @brief Prints the statistics of the benchmark
*/
void printStats(long long N, int threads, float elapsed) {
	fprintf(stderr, "%lli,%i,%f\n", N, threads, elapsed);
}
