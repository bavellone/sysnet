#define MAX_ARGS 32

// Make sure our struct is defined only once
#ifndef context_struct_h
#define context_struct_h

typedef struct context {
	char *inputRedirect;
	char *outputRedirect;
	int background;
	int argumentCount;
	char *argumentVector[MAX_ARGS];
} *Context;

#endif

// Prototypes
Context parse(char *);
Context newContext();
void inputArgument(char *, Context);
void printContext(Context);
void processSpecial(char*, Context, char *);
