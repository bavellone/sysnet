#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "myshell.h"

int DEBUG = 0;
int children = 0;

int main(int argc, char* argv[]) {
	// -Debug flag must be passed as first arg
	if (argc > 1 && strcmp(argv[1], "-Debug") == 0) {
		printf("Debug mode ON!\n");
		DEBUG = 1;
	}

	// Create a buffer to store the user input
	char promptBuffer[PROMPT_BUFFER_SIZE];

	// Loop until user exits or error occurs
	while (promptLoop(promptBuffer));

	// Make sure all children have died before terminating
	waitForChildren();
    printf("Goodbye!\n");
	return 0;
}

// The main loop
// Reads user input and passes it to lower level functions
int promptLoop(char *buffer) {
	// Only print prompt if
	if (isatty(fileno(stdin)))
		printf("myshell >> ");

	// Wait for user input
	// Allow space for newline char
	fgets(buffer, PROMPT_BUFFER_SIZE, stdin);

	// Check if we are finished reading from stdin
	if (isEOF())
		return 0;

	// Strip newline char automatically inserted by fgets
	stripNewline(buffer);

	// Parse user input
	return parseInput(buffer);
}

// Returns true if stdin is reading from a file and has reached the end
int isEOF() {
	if (feof(stdin))
		return 1;
	else
		return 0;
}

// Waits until all children die
void waitForChildren() {
	while (children > 0) {
		printf("Awaiting death of %i more children\n", children);
		wait(NULL);
		children--;
	}
}

// Removes newline char from the end of str
void stripNewline(char *str) {
	int len = strlen(str);
	char lastChar = str[len - 1];
    if (len > 1 && lastChar == '\n')
        lastChar = '\0';
}

// Returns 0 if user chooses to exit
// Returns 1 otherwise
int parseInput(char *str) {
	// Parse user input
	Context ctx = parse(str);

	// Check if user wants to exit
	if (checkExit(ctx))
		return 0;

	// Print context if debug flag is ON
	if (DEBUG)
		printContext(ctx);

	// Execute user input
	if (ctx->argumentCount > 0)
		createChild(ctx);

	return 1;
}

// Returns 1 if str is 'exit'
int checkExit(Context ctx) {
	if (ctx->argumentCount > 0 && strcmp(ctx->argumentVector[0], "exit") == 0)
		return 1;
	else
		return 0;
}

// Create a new child and wait() on it unless user chooses to run in background
void createChild(Context ctx) {
	pid_t pid;
	// Create a new process
	pid = fork();

	// Create variables for the child to reference, so we can clean up
	// our Context structure
	char **argv = ctx->argumentVector;
	char *input = ctx->inputRedirect;
	char *output = ctx->outputRedirect;

	// Am I the child?
	if (pid == 0) {
		executeContext(argv, input, output);
	}

	// Or the parent?
	else if (pid > 0) {
		if (!ctx->background)
			wait(NULL); // Wait for child if running in foreground
		else
			// We have one more child
            children++;
	}

	// Or did the call fail?
	else
		fprintf(stderr, "Could not create child process!\n");
}

// Execute the user input
void executeContext(char **argv, char *input, char *output) {
	// Redirect I/O if necessary
	// Make sure no errors occurred opening the file descriptors
	if (redirect(input, output)) {
		// Replace this process with the user specified executable or script with shebang
    	execvp(argv[0], argv);

    	// Only executed if call to execvp above fails
        fprintf(stderr, "Error: %s not found\n", argv[0]);
	}

	// Kill this child
    exit(1);
}

// Returns 0 if an error occurs while opening a file
// Returns 1 otherwise
int redirect(char *input, char *output) {
	if (input != NULL) {
		FILE *in = freopen(input, "r", stdin);
		if (in == NULL) {
			fprintf(stderr, "Error opening file: %s\n", input);
			return 0;
		}
	}
	if (output != NULL) {
        FILE *out = freopen(output, "w", stdout);
        if (out == NULL) {
            fprintf(stderr, "Error opening file: %s\n", output);
            return 0;
        }
    }
	return 1;
}
